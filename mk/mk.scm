(import (rnrs) (mk-r6rs core))

(define rsc3-midi-file-seq
  (list
   "byte.scm"
   "constants.scm"
   "event.scm"
   "file.scm"
   "general.scm"
   "mmc.scm"
   "read.scm"
   "time.scm"
   "write.scm"))

(define at-src
  (lambda (x)
    (string-append "../src/" x)))

(mk-r6rs '(rsc3 midi)
	 (map at-src rsc3-midi-file-seq)
	 (string-append (list-ref (command-line) 1) "/rsc3/midi.sls")
	 '((rnrs) (rhs core) (sosc core) (rsc3 core))
	 '()
	 '())

(exit)
