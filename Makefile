GL_GIT=git@gitlab.com:rd--/rsc3-midi.git
GL_HTTP=https://gitlab.com/rd--/rsc3-midi.git

all:
	(cd mk; make all)

clean:
	(cd mk; make clean)

push-gl:
	git push $(GL_GIT)

pull-gl:
	git pull $(GL_HTTP)

push-tags:
	git push $(GL_GIT) --tags

update-rd:
	ssh rd@rohandrape.net "(cd sw/rsc3-midi ; git pull $(GL_HTTP))"

push-all:
	make push-gl update-rd
