rsc3-midi
---------

scheme [midi](http://www.midi.org/midi) i/o

midi is a standard musical instrument digital interface and file format

© [rohan drape](http://rohandrape.net), 2002-2022, [gpl](http://gnu.org/copyleft/)
