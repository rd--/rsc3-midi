; r6rs
(import (rnrs) (rhs core) (sosc core) (rsc3 core) (rsc3 server) (rsc3 ugen) (rsc3 midi))

; type-1 midi-file
(define m-1080 "/home/rohan/sw/rsc3-midi/help/1080-C01.midi")

; midi-file
(define m (midi-file-read-file m-1080))

; [int, int, int]
(equal? (midi-file-header m) (list 1 17 120))

; int
(= (midi-file-format-type m) 1)

; int
(= (midi-file-number-of-tracks m) 17)

; int
(= (midi-file-division m) 120)

; type event = [float, [int]]

; [event]
(midi-file-track m 0)

; event -> bool
(define is-note-on?
  (lambda (e)
    (let* ((d (list-ref e 1))
           (s (car d)))
      (= mc-note-on (midi-status-major s)))))

; event -> event -> ord
(define event-compare
  (lambda (e1 e2)
    (compare (car e1) (car e2))))

; [event] -> [event]
(define to-absolute
  (lambda (l f)
    (let ((t (map f (map car l)))
          (m (map cadr l)))
      (zipWith
       list
       (cdr (integrate (cons 0 t)))
       m))))

; midi-file -> int -> float -> [event]
(define note-list-abs
  (lambda (d n i)
    (sortBy
     event-compare
     (concat
      (map (lambda (track)
             (filter is-note-on?
                     (to-absolute
                      (list-ref d track)
                      (lambda (t) (inexact (/ t i))))))
           (enumFromTo 1 (- n 1)))))))

; [event]
(define nl
  (note-list-abs
   m
   (midi-file-number-of-tracks m)
   (* 4 (midi-file-division m))))

; ugen
(define ping
  (letc ((freq 440)
         (ampl 0.1))
    (let ((p (envPerc 0.1 1 1 (list -4 -4))))
      (Out 0 (Mul (SinOsc (Mce2 freq (Add freq (Rand 1 2))) 0)
                  (EnvGen 1 ampl 0 1 removeSynth p))))))

; ()
(withSc3
 (lambda (fd)
   (sendSynth fd "ping" ping)
   (sendMessage fd (s_new2 "ping" -1 1 1
                            "freq" (s:midi-cps (s:rand 45 65))
                            "ampl" (s:rand 0.05 0.25)))))

; socket -> float -> event -> ()
;
; (withSc3 (lambda (fd) (ping-midi fd (+ (utcr) 2) (list '_ (list '_ 60 127)))))
(define ping-midi
  (lambda (fd t m)
    (let* ((d (list-ref m 1))
           (n (list-ref d 1))
           (g (list-ref d 2)))
      (sendBundle fd (bundle
                    t
                    (list
                     (s_new2 "ping" -1 0 1
                             "freq" (s:midi-cps n)
                             "ampl" (/ g 1280.0))))))))

; using only scsynth scheduler is straight-forward
; but can overflow the queue.  C-cC-k clears the
; schedule queue.
(withSc3
 (lambda (fd)
   (let ((d (midi-file-read-file m-1080))
         (t (utcr)))
     (for-each
      (lambda (m)
        (ping-midi fd (+ t (car m)) m))
      nl))))

; [event] -> [event]
;
; (to-relative nl)
(define to-relative
  (lambda (nl)
    (let ((f (lambda (pt m)
               (let ((t (car m))
                     (d (cadr m)))
                 (twoTuple t (list (- t pt) d))))))
      (snd (mapAccumL f 0 nl)))))

; using both scheme & scsynth scheduler is
; a little extra work but safer.  C-cC-i
; interrupts the scheme process.
(withSc3
 (lambda (fd)
   (let ((d (midi-file-read-file m-1080))
         (t (+ (utcr) 0.1)))
     (scanl
      (lambda (ct m)
        (ping-midi fd ct m)
        (thread-sleep (car m))
        (+ ct (car m)))
      t
      (to-relative nl)))))
