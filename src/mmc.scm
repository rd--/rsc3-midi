(define mmc-stop #x1)
(define mmc-play #x2)
(define mmc-deferred-play #x3)
(define mmc-fast-forward #x4)
(define mmc-rewind #x5)
(define mmc-record-strobe #x6)

(define mmc-record-exit #x7)
(define mmc-record-pause #x8)
(define mmc-pause #x9)
(define mmc-eject #xa)
(define mmc-chase #xb)
(define mmc-command-error-reset #xc)
(define mmc-reset #xd)

(define mmc-write #x40)
(define mmc-masked-write #x41)
(define mmc-read #x42)
(define mmc-update #x43)
(define mmc-locate #x44)
(define mmc-variable-play #x45)
(define mmc-search #x46)

(define mmc-shuttle #x47)
(define mmc-step #x48)
(define mmc-assign-system-master #x49)
(define mmc-generator-command #x4a)
(define mmc-mtc-command #x4b)
(define mmc-move #x4c)
(define mmc-add #x4d)

(define mmc-subtract #x4e)
(define mmc-drop-frame-adjust #x4f)
(define mmc-procedure #x50)
(define mmc-event #x51)
(define mmc-group #x52)
(define mmc-command-segment #x53)
(define mmc-deferred-variable-play #x54)

(define mmc-record-strobe-variable #x55)

(define mmc-wait #x7c)
(define mmc-resume #x7f)
