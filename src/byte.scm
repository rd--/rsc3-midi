; int -> int -> int
;
; (equal? (midi-combine-bytes 0 64) 8192)
; (equal? (midi-combine-bytes 127 127) 16383)
(define midi-combine-bytes
  (lambda (p q)
    (fxxor p (fxarithmetic-shift-left q 7))))

; int -> [int, int]
(define midi-uncombine-value
  (lambda (n)
    (list (fxand #x7f n)
	  (fxand #xff (fxarithmetic-shift-right n 7)))))

; int -> int -> int
(define midi-status-byte-construct
  (lambda (major minor)
    (fxxor major minor)))

; int -> int
(define midi-status-minor
  (lambda (n)
    (fxand n #x0f)))

; int -> int
(define midi-status-major
  (lambda (n)
    (fxand n #xf0)))

; int -> int -> bool
(define midi-status-check
  (lambda (type data)
    (= (fxand type data) type)))
