; port -> int -> [int]
(define read-u8-list
  (lambda (p n)
    (bytevector->u8-list
     (get-bytevector-n p n))))

; port -> string -> bool
(define confirm-magic
  (lambda (p magic)
    (let ((b (get-bytevector-n p 4)))
      (if (equal? (utf8->string b) magic)
	  #t
	  (error "confirm-magic" "magic not found" magic)))))

; port -> int -> int
(define read-integer
  (lambda (p n)
    (cond ((= n 2)
	   (read-i16 p))
	  ((= n 4)
	   (read-i32 p))
	  (else
	   (error "read-integer"
		  "illegal byte count"
		  n)))))

; port -> int
(define read-variable-length-integer
  (lambda (p)
    (let ((c (get-u8 p)))
      (if (= (fxand c #x80) 0)
	  c
	  (let loop ((a (fxand c #x7f)) (b (get-u8 p)))
	    (let ((d (+ (fxarithmetic-shift-left a 7) (fxand b #x7f))))
	      (if (= (fxand b #x80) 0)
		  d
		  (loop d (get-u8 p)))))))))

; port -> [int, int, int]
(define parse-header-chunk
  (lambda (p)
    (confirm-magic p "MThd")
    (let* ((bytes-to-read (read-i32 p))
	   (format (read-i16 p))
	   (number-of-tracks (read-i16 p))
	   (division (read-i16 p)))
      (if (> bytes-to-read 6)
	  (get-bytevector-n p (- bytes-to-read 6))
	  #f)
      (list format number-of-tracks division))))

; int -> maybe int
(define channel-message-arguments
  (lambda (status-byte)
    (let ((status-major (fxand status-byte #xf0)))
      (let ((result (assoc status-major '((#x80 2)
					  (#x90 2)
					  (#xa0 2)
					  (#xb0 2)
					  (#xc0 1)
					  (#xd0 1)
					  (#xe0 2)))))
	(if result
	    (list-ref result 1)
	    #f)))))

; port -> int -> int
(define read-status-byte
  (lambda (p last-status)
    (let ((next-byte (lookahead-u8 p)))
      (if (= 0 (fxand next-byte #x80))
	  (if (= last-status 0)
	      (error "read-status-byte"
		     "unexpected running status"
		     last-status next-byte)
	      last-status)
	  (get-u8 p)))))

; port -> int ->
(define parse-possible-multipart-sysex-event
  (lambda (p delta-time)
    (letrec ((iterator
	      (lambda (result)
		(let* ((message-length (read-variable-length-integer p))
		       (data-bytes (read-u8-list p message-length)))
		  (if (= (last data-bytes) #xf7)
		      (append result data-bytes)
		      (let ((unused-delta-time (read-variable-length-integer p))
			    (sysex-continue-status-byte (read-status-byte p #xf0)))
			(if (not (= sysex-continue-status-byte #xf7))
			    (error "parse-possible-multipart-sysex-event"
				   "un-continued sysex")
			    #f)
			(iterator (append result data-bytes))))))))
      (list delta-time (cons #xf0 (iterator '()))))))


; port -> int -> [int, [int]]
(define parse-track-event
  (lambda (p last-status)
    (let* ((delta-time (read-variable-length-integer p))
	   (status-byte (read-status-byte p last-status)))
      (let ((channel-arguments (channel-message-arguments status-byte)))
	(if channel-arguments
	    (let ((first-data-byte (get-u8 p)))
	      (if (= channel-arguments 1)
		  (list delta-time (list status-byte
					 first-data-byte))
		  (list delta-time (list status-byte
					 first-data-byte
					 (get-u8 p)))))
	    (case status-byte
	      ((#xff)
	       (let ((meta-event-type (get-u8 p)))
		 (list delta-time
		       (cons status-byte
			     (cons meta-event-type
				   (read-u8-list
				    p
				    (read-variable-length-integer p)
				    ))))))
	      ((#xf0)
	       (parse-possible-multipart-sysex-event p delta-time))
	      ((#xf7)
	       (list delta-time
		     (cons status-byte
			   (read-u8-list
			    p
			    (read-variable-length-integer p)
			    ))))
	      (else
	       (error "parse-track-event"
		      "illegal status byte"
		      status-byte))))))))

; port -> tree int
(define parse-track-chunk
  (lambda (p)
    (letrec ((iterator
	      (lambda (last-status result)
		(if (eof-object? (lookahead-u8 p))
		    (reverse result)
		    (let ((next-event (parse-track-event p last-status)))
		      (iterator (caadr next-event)
				(cons next-event result)))))))
      (iterator 0 '()))))

; port -> tree int
(define midi-file-read
  (lambda (p)
    (let ((hdr (parse-header-chunk p)))
      (append (list hdr)
	      (map (lambda (_)
		     (confirm-magic p "MTrk")
		     (let ((b (get-bytevector-n p (read-i32 p))))
		       (with-input-from-bytevector
			b
			parse-track-chunk)))
		   (enumFromTo 1 (list-ref hdr 1)))))))

; string -> tree int
(define midi-file-read-file
  (lambda (file-name)
    (let* ((p (open-file-input-port file-name))
	   (r (midi-file-read p)))
      (close-port p)
      r)))
