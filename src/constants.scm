; http://www.midi.org/techspecs/midimessages.php

; standard midi file meta event definitions.
(define mc-meta-event          #xff)
(define mc-sequence-number     #x00)
(define mc-text-event          #x01)
(define mc-copyright-notice    #x02)
(define mc-sequence-name       #x03)
(define mc-instrument-name     #x04)
(define mc-lyric               #x05)
(define mc-marker              #x06)
(define mc-cue-point           #x07)
(define mc-channel-prefix      #x20)
(define mc-end-of-track        #x2f)
(define mc-set-tempo           #x51)
(define mc-smpte-offset        #x54)
(define mc-time-signature      #x58)
(define mc-key-signature       #x59)
(define mc-sequencer-specific  #x74)

; midi channel commands.
(define mc-note-off            #x80)
(define mc-note-on             #x90)
(define mc-aftertouch          #xa0)
(define mc-poly-aftertouch     #xa0)
(define mc-control-change      #xb0)
(define mc-program-change      #xc0)
(define mc-channel-pressure    #xd0)
(define mc-channel-aftertouch  #xd0)
(define mc-pitch-wheel         #xe0)
(define mc-system-message      #xf0)
(define mc-non-voice-message   #xf0)

; seven bit controller names.
(define mc-bank-select         #x00)
(define mc-modulation-wheel    #x01)
(define mc-breath-controller   #x02)
(define mc-foot-pedal          #x04)
(define mc-portamento-time     #x05)
(define mc-data-entry          #x04)
(define mc-volume              #x07)
(define mc-balance             #x08)
(define mc-pan-position        #x0a)
(define mc-expression          #x0b)
(define mc-sustain-pedal       #x40)
(define mc-hold-pedal          #x40)
(define mc-portamento          #x41)
(define mc-sostenuto           #x42)
(define mc-soft-pedal          #x43)
(define mc-general-4           #x44)
(define mc-hold-2              #x45)
(define mc-general-5           #x50)
(define mc-general-6           #x51)
(define mc-general-7           #x52)
(define mc-general-8           #x53)
(define mc-tremolo-depth       #x5c)
(define mc-chorus-depth        #x5d)
(define mc-detune              #x5e)
(define mc-phaser-depth        #x5f)

; fourteen bit controller names.
(define mc-modulation-wheel-fine  #x21)
(define mc-breath-controller-fine #x22)
(define mc-data-entry-fine        #x26)
(define mc-volume-fine            #x27)
(define mc-balance-fine           #x28)
(define mc-pan-position-fine      #x2a)
(define mc-expression-fine        #x2b)

; system messages (minor part of status byte)
(define mc-system-exclusive      #x00)
(define mc-sysex                 #x00)
(define mc-mtc-quarter-frame     #x01)
(define mc-song-position-pointer #x02)
(define mc-song-select           #x03)
(define mc-tune-request          #x06)
(define mc-midi-clock            #x08)
(define mc-midi-tick             #x09)
(define mc-midi-start            #x0a)
(define mc-midi-continue         #x0b)
(define mc-midi-stop             #x0c)
(define mc-active-sense          #x0e)

; realtime messages
(define mc-timing   #xf8)
(define mc-start    #xfa)
(define mc-continue #xfb)
(define mc-stop     #xfc)
(define mc-reset    #xff)

; manufacturer's id number.
(define mc-seq-circuits        #x01) ; sequential circuits inc.
(define mc-big-briar           #x02) ; big briar inc.
(define mc-octave              #x03) ; octave/plateau
(define mc-moog                #x04) ; moog music
(define mc-passport            #x05) ; passport designs
(define mc-lexicon             #x06) ; lexicon
(define mc-tempi               #x20) ; bon tempi
(define mc-siel                #x21) ; s.i.e.l.
(define mc-kawai               #x41)
(define mc-roland              #x42)
(define mc-korg                #x42)
(define mc-yamaha              #x43)
