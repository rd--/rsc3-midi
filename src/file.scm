; tree int -> [int, int, int]
(define midi-file-header
  (lambda (data)
    (list-ref data 0)))

; tree int -> int
(define midi-file-format-type
  (lambda (data)
    (list-ref (midi-file-header data) 0)))

; tree int -> int
(define midi-file-number-of-tracks
  (lambda (data)
    (list-ref (midi-file-header data) 1)))

; tree int -> int
(define midi-file-division
  (lambda (data)
    (list-ref (midi-file-header data) 2)))

; tree int -> int -> tree int
(define midi-file-track
  (lambda (data track)
    (if (>= track (midi-file-number-of-tracks data))
	(error "midi-file-track"
	       "no such track in file"
	       track)
	#f)
    (list-ref data (+ track 1))))
